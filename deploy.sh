#!/bin/bash
DEPLOYHOST="root@138.197.185.68"

ssh -t $DEPLOYHOST "
  cd /var/www/careful-web-app
  git fetch --all && git reset --hard && git pull
  yarn install
  yarn build
  rm -rf productionBuild/
  cp -a build/ productionBuild/
"
export const API_URL = 'http://api.careful-store.com';
export const WEB_APP_URL = 'http://careful-store.com';
// export const API_URL = 'http://localhost:4000';
// export const WEB_APP_URL = 'http://localhost:3000';
export const NOVA_POSHTA_API_URL = 'https://api.novaposhta.ua/v2.0/json';

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const PHONE_REGEX = /^(\+?38)?[0-9]{10}$/;
export const LIQPAY_PUBLIC_KEY = 'i94267545983';
export const LIQPAY_PRIVATE_KEY = 'ghuPZdh75m1xphjh1erWLsImUHYpMfjs8rCkDnYW';

export const COLORS = {
  MAIN_COLOR: '#A8BF80',
  DARK_GREY: '#777',
  LIGHT_GRAY: '#ccc',
  VERY_LIGHT_GRAY: '#efefef',
  LIGHT_RED: '#F7B5B5',
  GRAY: '#999'
};

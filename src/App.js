import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from './components/header';
import MainScreen from './screens/main';
import CategoryScreen from './screens/category';
import ContactsScreen from './screens/contacts';
import PaymentAndDeliveryScreen from './screens/paymentAndDelivery';
import PrivacyPolicyScreen from './screens/privacyPolicy';
import AboutUs from './screens/aboutUs';
import PersonalInfo from './screens/checkout/personalInfo';
import ContractOffer from './screens/contractOffer';
import Delivery from './screens/checkout/delivery';
import Summary from './screens/checkout/summary';
import ProductScreen from './screens/product';
import store from './store';
import CartModal from './components/cartModal';
import SearchBar from './components/searchBar';
import PostLiqpay from './screens/checkout/postLiqpay';
import ScrollToTop from './components/scrollToTop';
import Menu from './components/menu';
import API from './api';
import { addCategories } from './actions/categeories';
import { addProducts } from './actions/products';
import { removeProductFromCart } from './actions/cart';

class AppRouter extends Component {
  state = {
    cartModalVisible: false,
    menuVisible: false,
    searchBarVisible: false
  };

  async componentDidMount() {
    this.getCategories();
    this.getProducts();
  }

  getProducts = async () => {
    const { data } = await API.getProductsList();
    if (!data.success) return;
    // this.removeOldProductsFromCart();
    store.dispatch(addProducts({ products: data.payload.products }));
  };

  getCategories = async () => {
    const { data } = await API.getCategoriesList();
    if (!data.success) return;
    store.dispatch(addCategories({ categories: data.payload.categories }));
  };

  removeOldProductsFromCart = () => {
    const state = store.getState();
    state.cart.products
      .filter(product => state.products[product.id] == null)
      .forEach(product => store.dispatch(removeProductFromCart({ id: product.id })));
  };

  showSearchBar = () => this.setState({ searchBarVisible: true });
  hideSearchBar = () => this.setState({ searchBarVisible: false });

  showCartModal = () => this.setState({ cartModalVisible: true });
  hideCartModal = () => this.setState({ cartModalVisible: false });

  showMenu = () => this.setState({ menuVisible: true });
  hideMenu = () => this.setState({ menuVisible: false });

  render() {
    return (
      <Provider store={store}>
        <Router onUpdate={() => window.scrollTo(0, 0)}>
          <div>
            <Header onMenuPress={this.showMenu} onCartPress={this.showCartModal} onLoopPress={this.showSearchBar} />
            <SearchBar close={this.hideSearchBar} visible={this.state.searchBarVisible} />
            <CartModal close={this.hideCartModal} visible={this.state.cartModalVisible} />
            <Menu close={this.hideMenu} visible={this.state.menuVisible} />
            <div style={{ marginTop: '60px' }}>
              <Route path='/' exact component={MainScreen} />
              <Route path='/product/:id' exact component={() => <ProductScreen showCartModal={this.showCartModal} />} />
              <Route path='/category/:id' exact component={CategoryScreen} />
              <Route path='/checkout/personal-info' exact component={PersonalInfo} />
              <Route path='/checkout/delivery' exact component={Delivery} />
              <Route path='/checkout/summary' exact component={() => <Summary showCartModal={this.showCartModal} />} />
              <Route path='/contacts' exact component={ContactsScreen} />
              <Route path='/payment-and-delivery' exact component={PaymentAndDeliveryScreen} />
              <Route path='/privacy-policy' exact component={PrivacyPolicyScreen} />
              <Route path='/about-us' exact component={AboutUs} />
              <Route path='/contract-offer' exact component={ContractOffer} />
              <Route path='/checkout/post-liqpay' exact component={PostLiqpay} />
            </div>
            <ScrollToTop />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default AppRouter;

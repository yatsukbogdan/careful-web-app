export const ADD_CATEGORIES = 'ADD_CATEGORIES';

export const addCategories = (payload) => ({
  type: ADD_CATEGORIES,
  payload
})
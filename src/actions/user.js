export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';

export const updateUserInfo = (payload) => ({
  type: UPDATE_USER_INFO,
  payload
});
export const ADD_PRODUCT_TO_CART = 'ADD_PRODUCT_TO_CART';
export const INCREMENT_PRODUCT_QUANTITY = 'INCREMENT_PRODUCT_QUANTITY';
export const DECREMENT_PRODUCT_QUANTITY = 'DECREMENT_PRODUCT_QUANTITY';
export const REMOVE_PRODUCT_FROM_CART = 'REMOVE_PRODUCT_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const UPDATE_PRODUCT_IN_CART = 'UPDATE_PRODUCT_IN_CART';

export const addProductToCart = (payload) => ({
  type: ADD_PRODUCT_TO_CART,
  payload
});

export const incrementProductQuantity = (payload) => ({
  type: INCREMENT_PRODUCT_QUANTITY,
  payload
});

export const decrementProductQuantity = (payload) => ({
  type: DECREMENT_PRODUCT_QUANTITY,
  payload
});

export const removeProductFromCart = (payload) => ({
  type: REMOVE_PRODUCT_FROM_CART,
  payload
});

export const clearCart = () => ({ type: CLEAR_CART })

export const updateProductInCart = (payload) => ({
  type: UPDATE_PRODUCT_IN_CART,
  payload
});
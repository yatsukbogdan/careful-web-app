import axios from 'axios';
import { API_URL, NOVA_POSHTA_API_URL } from '../config';

export default class API {
  static init() {
    const api = axios.create({ baseURL: API_URL });
    this.api = api;
  }

  static getProductsList(categoryId) {
    if (!categoryId) return this.api.get('/product/list');
    return this.api.get(`/product/list?categoryId=${categoryId}`);
  }
  static getCountriesList() {
    return this.api.get('/country/list');
  }
  static getCitiesList(input) {
    return this.api.get(`/rozetka/cities?name=${input ? input : 'а'}`);
  }
  static getStreetsList(input, cityId) {
    return this.api.get(`/rozetka/streets?cityId=${cityId}&name=${input ? input : 'а'}`);
  }
  static getCategoriesList() {
    return this.api.get('/category/list');
  }
  static getProduct(id) {
    return this.api.get(`/product/${id}`);
  }
  static checkInvoiceStatus(invoiceId) {
    return this.api.get(`/invoice/${invoiceId}/status`);
  }
  static addCustomer(body) {
    return this.api.post('/customer', body);
  }
  static addInvoice(body) {
    return this.api.post('/invoice', body);
  }
  static addDelivery(body) {
    return this.api.post('/delivery', body);
  }
  static addProductFeedback(productId, body) {
    return this.api.put(`/product/${productId}/feedback`, body);
  }
}

export class NovaPoshtaAPI {
  static init() {
    const api = axios.create({ baseURL: NOVA_POSHTA_API_URL });
    this.api = api;
  }

  static getCities(input) {
    return this.api.post('/Address/getCities', {
      modelName: 'Address',
      calledMethod: 'getCities',
      methodProperties: {
        FindByString: input ? input.trim() : ''
      }
    });
  }

  static getWarehouses(cityId) {
    return this.api.post('/AddressGeneral/getWarehouses', {
      modelName: 'AddressGeneral',
      calledMethod: 'getWarehouses',
      methodProperties: {
        Language: 'ru',
        CityRef: cityId
      }
    });
  }
}

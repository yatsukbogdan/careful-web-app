import { UPDATE_USER_INFO } from '../actions/user';

const initialState = {}

export default (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_USER_INFO: return {
      ...state,
      ...action.payload
    }
    default:
    return state;
  }
}
import { ADD_PRODUCT, ADD_PRODUCTS } from '../actions/products';

const initialState = {}

const normilizeProducts = products => 
  products.reduce((obj, product) => {
    obj[product.id] = product;
    return obj;
  }, {})

export default (state = initialState, action) => {
  switch(action.type) {
    case ADD_PRODUCT: {
      const { product } = action.payload;

      return {
        ...state,
        [product.id]: product
      }
    }
    case ADD_PRODUCTS: return {
      ...state,
      ...normilizeProducts(action.payload.products)
    }
    default:
    return state;
  }
}
import { ADD_CATEGORIES } from '../actions/categeories';

const initialState = {}

const normilizeCategories = categories => 
  categories.reduce((obj, cat) => {
    obj[cat.id] = cat;
    return obj;
  }, {})

export default (state = initialState, action) => {
  switch(action.type) {
    case ADD_CATEGORIES: return {
      ...state,
      ...normilizeCategories(action.payload.categories)
    }
    default:
    return state;
  }
}
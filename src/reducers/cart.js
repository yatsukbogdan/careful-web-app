import {
  ADD_PRODUCT_TO_CART,
  UPDATE_PRODUCT_IN_CART,
  REMOVE_PRODUCT_FROM_CART,
  INCREMENT_PRODUCT_QUANTITY,
  DECREMENT_PRODUCT_QUANTITY,
  CLEAR_CART
} from '../actions/cart';

const initialState = {
  products: []
}

const getProductIndex = (id, products) => products.findIndex(product => product.id === id);

const incrementedProductsArray = (index, by, products) => [
  ...products.slice(0, index),
  {
    ...products[index],
    quantity: products[index].quantity + by
  },
  ...products.slice(index + 1)
]

export default (state = initialState, action) => {
  switch(action.type) {
    case ADD_PRODUCT_TO_CART: {
      const { id } = action.payload;

      return {
        ...state,
        products: [...state.products, { id, quantity: 1 }]
      }
    }
    case UPDATE_PRODUCT_IN_CART:
    case REMOVE_PRODUCT_FROM_CART: {
      const index = getProductIndex(action.payload.id, state.products)

      if (index === -1) return state;

      return {
        ...state,
        products: [
          ...state.products.slice(0, index),
          ...state.products.slice(index + 1)
        ]
      }
    }
    case CLEAR_CART:
      return {
        ...state,
        products: []
      }
    case INCREMENT_PRODUCT_QUANTITY: {
      const index = getProductIndex(action.payload.id, state.products)

      if (index === -1) return state;

      return {
        ...state,
        products: incrementedProductsArray(index, 1, state.products)
      }
    }
    case DECREMENT_PRODUCT_QUANTITY: {
      const index = getProductIndex(action.payload.id, state.products)

      if (index === -1) return state;

      return {
        ...state,
        products: incrementedProductsArray(index, -1, state.products)
      }
    }
    default:
    return state;
  }
}
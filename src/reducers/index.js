import cart from './cart';
import products from './products';
import categories from './categories';
import user from './user';
import { combineReducers } from 'redux';

export default combineReducers({
  cart,
  categories,
  products,
  user
})
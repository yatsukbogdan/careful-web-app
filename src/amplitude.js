import amplitude from 'amplitude-js/amplitude';

const findGetParameter = parameterName => {
  const items = window.location.search.substr(1).split('&');
  for (let i = 0; i < items.length; i++) {
    const [key, value] = items[i].split('=');
    if (key === parameterName) return value;
  }
  return null;
};

export const initAmplitude = () => {
  const amplitudeInstance = amplitude.getInstance();
  amplitudeInstance.init('31290a87b94e8dad9005d3027aea0f94');
  amplitudeInstance.logEvent('sessionStart', { path: window.location.pathname });
  const source = findGetParameter('source');
  const sourceFromLocalStorage = localStorage.getItem('source');
  if (sourceFromLocalStorage !== null || source === null) return;
  localStorage.setItem('source', source);
  amplitudeInstance.setUserProperties({ source });
};

export const setAmplitudeUserDevice = installationToken => {
  amplitude.getInstance().setDeviceId(installationToken);
};

export const setAmplitudeUserId = userId => {
  amplitude.getInstance().setUserId(userId);
};

export const setAmplitudeUserProperties = properties => {
  amplitude.getInstance().setUserProperties(properties);
};

export const sendAmplitudeData = (eventType, eventProperties) => {
  amplitude.getInstance().logEvent(eventType, eventProperties);
};

export const getObjectFromCartProductsArray = products =>
  products.reduce((obj, prod) => {
    obj[prod.name] = prod.quantity;
    return obj;
  }, {});

import React, { Component } from 'react';
import styled from 'styled-components';
import Footer from '../../components/footer';

const Container = styled.div`
  padding: 20px;
`;

const BigText = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 20px;
`;

const Text = styled.p`
  line-height: 25px;
  margin-bottom: 10px;
`;

const CardMethodContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const PaymentImage = styled.img`
  margin-left: 10px;
  height: 30px;
`;

const PaymentsContainer = styled.div`
  padding-left: 20px;
  margin-bottom: 20px;
`;

export default class ContactsScreen extends Component {
  state = {
    error: null,
    loading: false
  };

  render() {
    return (
      <div>
        <Container>
          <BigText>Оплата и доставка</BigText>
          <p style={{ fontWeight: '500', marginBottom: '15px' }}>Способы оплаты заказов:</p>
          <PaymentsContainer>
            <CardMethodContainer>
              <Text>- Карта</Text>
              <PaymentImage
                alt='MasterCard'
                src='https://picua.org/images/2018/11/11/61921fd3b6ccd7d3fb4d67918133d44a.png'
              />
            </CardMethodContainer>
            <Text>- Приват24</Text>
            <Text>- LiqPay</Text>
            <Text>- MasterPass</Text>
          </PaymentsContainer>
          <Text style={{ lineHeight: '25px' }}>
            Доставка заказов по Украине осуществляется при помощи сервисов "Новая почта" и "Укрпочта".
          </Text>
          <Text>Доставку оплачивает получатель, согласно тарифам перевозчика.</Text>
          <Text> Доставка за пределы Украины на данный момент не осуществляется.</Text>
        </Container>
        <Footer />
      </div>
    );
  }
}

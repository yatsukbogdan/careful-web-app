import React, { Component } from 'react';
import Album from './components/album';
import Info from './components/info';
import Footer from '../../components/footer';
import styled from 'styled-components';
import API from '../../api';
import { addProduct } from '../../actions/products';
import { addProductToCart } from '../../actions/cart';
import { sendAmplitudeData } from '../../amplitude';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { media } from '../../utils';
import Feedbacks from './components/feedbacks';
import FeedbackSuccessModal from './components/feedbackSuccessModal';

const Container = styled.div`
  ${media.retina`width: 30%;`};
  ${media.tablet`width: 50%;`};
  ${media.phone`width: 100%;`};
  transform: translate(-50%, 0);
  margin-left: 50%;
`;

class ProductScreen extends Component {
  state = {
    loading: false,
    error: null,
    successModalVisible: false
  };

  async componentDidMount() {
    const { id } = this.props.match.params;
    this.setState({ loading: true });
    const { data } = await API.getProduct(id);
    this.setState({ loading: false });

    if (!data.success) return this.setState({ error: data.message });

    this.props.actions.addProduct({ product: data.payload.product });
  }

  showSuccessModal = () => {
    this.setState({ successModalVisible: true });
    setTimeout(this.hideSuccessModal, 2000);
  };

  hideSuccessModal = () => this.setState({ successModalVisible: false });

  onBuyPress = () => {
    const { id } = this.props.match.params;
    const indexInCart = this.props.cartProducts.findIndex(product => product.id === id);
    if (indexInCart === -1) this.props.actions.addProductToCart({ id });
    sendAmplitudeData('onBuyProductClick', { id, name: this.props.product ? this.props.product.name : 'unknown' });
    this.props.showCartModal();
  };

  render() {
    const { product } = this.props;

    if (!product) return null;

    return (
      <Container>
        <Album album={product.album} />
        <Info onBuyPress={this.onBuyPress} product={product} />
        <Feedbacks onFeedbackSentSuccess={this.showSuccessModal} productId={product.id} feedbacks={product.feedbacks} />
        <Footer />
        <FeedbackSuccessModal visible={this.state.successModalVisible} close={this.hideSuccessModal} />
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  product: state.products[ownProps.match.params.id],
  cartProducts: state.cart.products
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      addProduct,
      addProductToCart
    },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ProductScreen)
);

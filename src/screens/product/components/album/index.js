import React, { Component } from 'react';
import styled from 'styled-components';
import Slider from 'react-slick';
import { COLORS } from '../../../../config';

const Container = styled.div`
  position: relative;
  padding: 20px 20px 0 20px;
  box-sizing: border-box;
  overflow: hidden;
`

const Image = styled.img`
  object-fit: cover;
`

const StripContainer = styled.div`
  position: relative;
  padding: 10px 20px;
  overflow-x: scroll;
  box-sizing: border-box;
  display: flex;
`

const StripImage = styled.img`
  height: 40px;
  width: 40px;
  box-sizing: border-box;
  margin-left: ${ props => props.first ? '0' : '5px'};
  border: ${ props => props.active ? `3px ${ COLORS.LIGHT_GRAY } solid` : '3px transparent solid'};
  object-fit: cover;
`

export default class Product extends Component {
  state = {
    activeSlideIndex: 0
  }

  onSliderAfterChange = index => this.setState({activeSlideIndex: index})
  setCurrentSlide = index => this.slider.slickGoTo(index);

  render() {
    const { album } = this.props;
    
    return (
        <div>
          <Container>
            <Slider
              ref={ slider => this.slider = slider}
              initialSlide={0}
              slide={this.state.activeSlideIndex}
              afterChange={this.onSliderAfterChange}
              arrows={false}
              infinite
              autoplay
              speed={500}
              slidesToShow={1}
              slidesToScroll={1}
            >
              {album.map(imageUrl => <Image src={imageUrl}/>)}
            </Slider>
          </Container>
          <StripContainer>
            {album.map((imageUrl, index) => (
              <StripImage
                first={index === 0}
                active={index === this.state.activeSlideIndex}
                onClick={() => this.setCurrentSlide(index)}
                src={imageUrl}
              />
            ))}
          </StripContainer>
        </div>
    )
  }
}
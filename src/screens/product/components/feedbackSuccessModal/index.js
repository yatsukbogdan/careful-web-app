import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { MdDone } from 'react-icons/md';
import { COLORS } from '../../../../config';

const OuterContainer = styled.div`
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  z-index: ${props => (props.visible ? 999999999999 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  align-items: center;
  justify-content: center;
  display: flex;
  transition: 0.4s;
  top: 0;
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
`;

const InnerContainer = styled.div`
  box-sizing: border-box;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  background-color: white;
  height: 30%;
  width: 90%;
  transition: 0.4s;
  transform: ${props => (props.visible ? 'translate(0, 0)' : 'translate(0, 100vh)')};
`;

const Text = styled.p`
  font-size: 16px;
  text-align: center;
`;

export default class FeedbackSuccessModal extends Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    document.body.appendChild(this.el);
  }

  render() {
    return ReactDOM.createPortal(
      <OuterContainer onClick={this.props.close} visible={this.props.visible}>
        <InnerContainer onClick={ev => ev.stopPropagation()} visible={this.props.visible}>
          <MdDone style={{ marginBottom: '20px' }} size={40} color={COLORS.MAIN_COLOR} />
          <Text>Ваш отзыв успешно отправлен и будет опубликован после модерации. Спасибо!</Text>
        </InnerContainer>
      </OuterContainer>,
      this.el
    );
  }
}

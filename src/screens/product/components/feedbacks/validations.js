export const nameValidation = name => {
  if (name.length === 0) return { success: false, error: 'Поле Имя не может быть пустым' };
  return { success: true };
}

export const textValidation = text => {
  if (text.length === 0) return { success: false, error: 'Поле Коментарий не может быть пустым' };
  return { success: true };
}
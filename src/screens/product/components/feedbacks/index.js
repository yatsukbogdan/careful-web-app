import React, { Component } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import { COLORS } from '../../../../config';
import { sendAmplitudeData } from '../../../../amplitude';
import Input from '../../../../components/input';
import { nameValidation, textValidation } from './validations';
import Button from '../../../../components/button';
import API from '../../../../api';

const Container = styled.div`
  margin-top: 25px;
  padding: 0 25px 25px 25px;
`;

const HeaderText = styled.p`
  font-size: 20px;
  flex: 1;
`;

const FeedbackContainer = styled.div``;

const NameDateContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const Divider = styled.div`
  height: 1px;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
  margin: 15px 0;
`;

const NameText = styled.p`
  font-size: 16px;
`;

const DateText = styled.p`
  font-size: 14px;
  color: ${COLORS.GRAY};
`;

const FeedbackText = styled(NameText)`
  opacity: 0.7;
`;

const NoFeedbacksContainer = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const NoFeedbacksText = styled.p`
  font-size: 16px;
  color: ${COLORS.GRAY};
`;

export default class Feedbacks extends Component {
  state = {
    nameInput: '',
    nameError: false,
    nameErrorText: '',

    textInput: '',
    textError: false,
    textErrorText: ''
  };

  componentWillUnmount() {
    if (this.state.textInput !== '') {
      sendAmplitudeData('unsentFeedback', { text: this.state.textInput });
    }
  }

  onFieldChange = (value, field) =>
    this.setState({
      [`${field}Input`]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: ''
    });

  validateField = async field => {
    let validationFunction;

    switch (field) {
      case 'name':
        validationFunction = nameValidation;
        break;
      case 'text':
        validationFunction = textValidation;
        break;
      default:
        validationFunction = nameValidation;
        break;
    }

    const validationObject = validationFunction(this.state[`${field}Input`]);

    if (validationObject.success) return;

    await this.setState({
      [`${field}Error`]: true,
      [`${field}ErrorText`]: validationObject.error
    });
  };

  validateAllFields = async () => {
    await Promise.all([this.validateField('name'), this.validateField('text')]);
  };

  onSendClick = async () => {
    await this.validateAllFields();
    const { nameError, textError } = this.state;

    if (nameError || textError) {
      if (nameError) this.nameInput.startErrorAnimation();
      if (textError) this.textInput.startErrorAnimation();
      return;
    }

    const { nameInput, textInput } = this.state;
    const { data } = await API.addProductFeedback(this.props.productId, {
      text: textInput,
      name: nameInput,
      customerId: localStorage.getItem('customerId')
    });
    if (!data.success) {
      alert(data.message);
      return;
    }
    this.setState({
      nameInput: '',
      textInput: ''
    });
    this.props.onFeedbackSentSuccess();
  };

  renderFeedbacks = () => {
    const { feedbacks } = this.props;

    if (feedbacks.length === 0) {
      return (
        <NoFeedbacksContainer>
          <NoFeedbacksText>Отзывов нет</NoFeedbacksText>
        </NoFeedbacksContainer>
      );
    }

    return (
      <div>
        <Divider />
        {feedbacks.map((feedback, index) => (
          <FeedbackContainer>
            {index !== 0 && <Divider />}
            <NameDateContainer>
              <NameText>{feedback.name}</NameText>
              <DateText>{moment(feedback.date).format('L')}</DateText>
            </NameDateContainer>
            <FeedbackText>{feedback.text}</FeedbackText>
          </FeedbackContainer>
        ))}
        <Divider />
      </div>
    );
  };

  render() {
    const { feedbacks } = this.props;

    return (
      <Container>
        <HeaderText>Отзывы ({feedbacks.length})</HeaderText>
        {this.renderFeedbacks()}
        <NameText style={{ marginBottom: '15px' }}>Оставить отзыв</NameText>
        <Input
          value={this.state.nameInput}
          label='Имя'
          onChange={value => this.onFieldChange(value, 'name')}
          onBlur={() => this.validateField('name')}
          ref={input => (this.nameInput = input)}
          errorVisible={this.state.nameError}
          errorText={this.state.nameErrorText}
        />
        <Input
          multiline={true}
          value={this.state.textInput}
          label='Отзыв'
          onChange={value => this.onFieldChange(value, 'text')}
          onBlur={() => this.validateField('text')}
          ref={input => (this.textInput = input)}
          errorVisible={this.state.textError}
          errorText={this.state.textErrorText}
        />
        <Button textStyle={{ fontSize: '14px' }} onClick={this.onSendClick} label='Оставить отзыв' />
      </Container>
    );
  }
}

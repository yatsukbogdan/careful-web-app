import React, { Component } from 'react';
import styled from 'styled-components';
import { numberToCurrency } from '../../../../utils';
import { COLORS } from '../../../../config';
import Button from '../../../../components/button';

const TextContainer = styled.div`
  display: flex;
  margin-bottom: 20px;
`

const Container = styled.div`
  padding: 0 25px 25px 25px;
`

const NameText = styled.p`
  font-size: 24px;
  flex: 1;
`

const PriceContainer = styled.p`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`

const Text = styled.p`
  text-align: center;
  letter-spacing: 1px;
  color: black;
`

const ProductDiscountPrice = styled(Text)`
  font-size: 20px;
  color: ${ COLORS.MAIN_COLOR };
`

const ProductPrice = styled(Text)`
  text-decoration: ${ props => props.discountPrice ? 'line-through' : 'none'};
  font-size: ${ props => props.discountPrice ? '12px': '20px'};
`

const DescriptionText = styled.p`
  white-space: pre-line;
  color: rgba(0, 0, 0, 0.7);
`

export default class Info extends Component {
  render() {
    const { product } = this.props;

    if (!product) return null;

    return (
      <Container>
        <TextContainer>
          <NameText>{ product.name }</NameText>
          <PriceContainer>
            { product.discountPrice && (
              <ProductDiscountPrice>
                { numberToCurrency(product.discountPrice, product.currency) }
              </ProductDiscountPrice>
            )}
            <ProductPrice discountPrice={product.discountPrice}>{numberToCurrency(product.price, product.currency)}</ProductPrice>
          </PriceContainer>
        </TextContainer>
        { product.balance > 0 ? (
          <Button
            onClick={ () => this.props.onBuyPress() }
            label="Купить"
            style={{ 'margin-bottom': '20px' }}
          />
        ): (
          <Button
            disabled={ true }
            label="Нет в наличии"
            style={{ 'margin-bottom': '20px' }}
          />
        )}
        <DescriptionText>{ product.description }</DescriptionText>
      </Container>
    )
  }
}
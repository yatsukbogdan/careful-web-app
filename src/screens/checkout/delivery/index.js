import React, { Component } from 'react';
import debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import API, { NovaPoshtaAPI } from '../../../api';
import { sendAmplitudeData } from '../../../amplitude';
import Button from '../../../components/button';
import Select from '../components/select';
import SelectWithInput from '../../../components/selectWithInput';
import Input from '../../../components/input';
import { deliveryOptions, mapNovaPoshtaCities, mapNovaPoshtaAdresses } from '../../../utils';
import { updateUserInfo } from '../../../actions/user';

const Container = styled.div`
  padding: 25px;
`;

const Header = styled.p`
  letter-spacing: 2px;
  margin-bottom: 25px;
  font-size: 24px;
`;

class Delivery extends Component {
  state = {
    deliveryId: this.props.deliveryId != null ? this.props.deliveryId : deliveryOptions[0].id,
    deliveryError: false,
    deliveryErrorText: '',

    countries: [],
    countryId: this.props.countryId != null ? this.props.countryId : null,
    countryError: null,
    countryErrorText: '',

    cities: [],
    citiesPending: false,
    cityInput: this.props.cityName != null ? this.props.cityName : '',
    cityId: this.props.cityId != null ? this.props.cityId : null,
    cityError: null,
    cityErrorText: '',

    // deliveryType: 0
    npps: [],
    nppsPending: false,
    nppInput: this.props.nppName != null ? this.props.nppName : '',
    nppId: this.props.nppId != null ? this.props.nppId : null,
    nppError: null,
    nppErrorText: '',

    // deliveryType: 1
    streets: [],
    streetsPending: false,
    streetInput: this.props.streetName != null ? this.props.streetName : '',
    streetId: this.props.streetId != null ? this.props.streetId : null,
    streetError: null,
    streetErrorText: '',

    houseInput: this.props.house != null ? this.props.house : '',
    houseError: null,
    houseErrorText: '',

    aptInput: this.props.apartment != null ? this.props.apartment : '',
    aptError: null,
    aptErrorText: '',

    postcodeInput: this.props.postcode != null ? this.props.postcode : '',
    postcodeError: null,
    postcodeErrorText: ''
  };

  componentDidMount() {
    this.getCountries();
    this.getCities();
  }

  // API REQUESTS

  getReleatedFields = field => {
    switch (field) {
      case 'delivery': {
        this.getCities();
        return;
      }
      case 'city': {
        if (this.state.deliveryId === 0) {
          this.getNPPs();
          return;
        }
        this.getStreets();
        return;
      }
      default:
        return;
    }
  };

  getCountries = async () => {
    const { data } = await API.getCountriesList();
    if (!data.success) {
      // TODO show popup
      alert(data.message);
      return;
    }
    const countries = data.payload.countries.map(country => ({ id: country.id, text: country.name }));

    this.setState({ countries });
    if (countries.length > 0) this.setCountryId(countries[0].id);
  };

  getCities = () => {
    if (this.state.deliveryId === 0) {
      this.getNovaPoshtaCities();
      return;
    }
    this.getRozetkaCities();
  };

  getRozetkaCities = debounce(async () => {
    this.setState({ citiesPending: true });
    const { data } = await API.getCitiesList(this.state.cityInput);
    this.setState({ citiesPending: false });
    if (!data.success) {
      this.setState({ cities: [] });
      return;
    }
    const cities = data.payload.cities.slice(0, 50);

    this.setState({ cities });
  }, 300);

  getNovaPoshtaCities = debounce(async () => {
    this.setState({ citiesPending: true });
    const { data } = await NovaPoshtaAPI.getCities(this.state.cityInput);
    this.setState({ citiesPending: false });
    if (!data.success) {
      this.setState({ cities: [] });
      return;
    }
    const cities = data.data.slice(0, 50).map(mapNovaPoshtaCities);
    this.setState({ cities });
  }, 300);

  getNPPs = async () => {
    this.setState({ nppsPending: true });
    const { data } = await NovaPoshtaAPI.getWarehouses(this.state.cityId);
    this.setState({ nppsPending: false });
    if (!data.success) {
      // TODO show popup
      this.setState({ npps: [] });
      return;
    }
    const points = data.data.map(mapNovaPoshtaAdresses);

    this.setState({ npps: points });
  };

  getStreets = debounce(async () => {
    this.setState({ streetsPending: true });
    const { data } = await API.getStreetsList(this.state.streetInput, this.state.cityId);
    this.setState({ streetsPending: false });
    if (!data.success) {
      // TODO show popup
      this.setState({ streets: [] });
      return;
    }
    const { streets } = data.payload;

    this.setState({ streets });
  }, 300);

  // VALIDATIONS

  validateUkrposhtaDeliveryFields = async () => {
    await Promise.all([
      this.validateCity(),
      this.validateStreet(),
      this.validateApt(),
      this.validateHouse(),
      this.validatePostcode()
    ]);
  };

  validateNovaPohstaDeliveryFields = async () => {
    await Promise.all([this.validateCity(), this.validateNPP()]);
  };

  validateCity = async () => {
    if (this.state.cityId !== null) return;

    await this.setState({
      cityError: true,
      cityErrorText: 'Город не выбран'
    });
  };

  validateStreet = async () => {
    if (this.state.streetId !== null) return;

    await this.setState({
      streetError: true,
      streetErrorText: 'Улица не вы'
    });
  };

  validateHouse = async () => {
    if (this.state.houseInput !== '') return;

    await this.setState({
      houseError: true,
      houseErrorText: 'Номер дома не указан'
    });
  };

  validateApt = async () => {
    if (this.state.aptInput !== '') return;

    await this.setState({
      aptError: true,
      aptErrorText: 'Номер квартиры не указан'
    });
  };

  validatePostcode = async () => {
    if (this.state.postcodeInput === '') {
      await this.setState({
        postcodeError: true,
        postcodeErrorText: 'Почтовый индекс не указан'
      });
      return;
    }

    const valid = /^[0-9]{5}$/.test(this.state.postcodeInput);
    if (valid) return;

    await this.setState({
      postcodeError: true,
      postcodeErrorText: 'Почтовый индекс указан не верно'
    });
  };

  validateNPP = async () => {
    if (this.state.nppId !== null) return;

    await this.setState({
      nppError: true,
      nppErrorText: 'Точка не выбрана'
    });
  };

  // FIELDS CLEAR FUNCTIONS

  clearField = field =>
    this.setState({
      [`${field}Input`]: '',
      [`${field}Error`]: null,
      [`${field}ErrorText`]: ''
    });

  clearSelectField = field =>
    this.setState({
      [`${field}Input`]: '',
      [`${field}Id`]: null,
      [`${field}Error`]: null,
      [`${field}ErrorText`]: ''
    });

  clearRelatedFields = field => {
    switch (field) {
      case 'delivery':
        this.clearSelectField('city');
        this.clearSelectField('npp');
        this.clearSelectField('street');
        break;
      case 'city': {
        if (this.state.deliveryId === 0) {
          this.clearSelectField('npp');
          return;
        }
        this.clearSelectField('street');
        break;
      }
      default:
        return;
    }
  };

  // INPUT HANDLERS

  setCountryId = countryId => this.setState({ countryId });
  setDeliveryId = deliveryId =>
    this.setState({ deliveryId }, () => {
      this.clearRelatedFields('delivery');
      this.getReleatedFields('delivery');
    });

  onSelectChange = (field, item, callback) =>
    this.setState(
      {
        [`${field}Input`]: item.text,
        [`${field}Id`]: item.id,
        [`${field}Error`]: null,
        [`${field}ErrorText`]: ''
      },
      () => {
        if (typeof callback === 'function') callback();
      }
    );

  onCityChange = city =>
    this.onSelectChange('city', city, () => {
      this.clearRelatedFields('city');
      this.getReleatedFields('city');
    });
  onNPPChange = npp => this.onSelectChange('npp', npp);
  onStreetChange = street => this.onSelectChange('street', street);

  onInputChange = (field, input, callback) =>
    this.setState(
      {
        [`${field}Input`]: input,
        [`${field}Error`]: null,
        [`${field}ErrorText`]: ''
      },
      () => {
        if (typeof callback === 'function') callback();
      }
    );

  onCityInputChange = input => this.onInputChange('city', input, this.getCities);
  onNPPInputChange = input => this.onInputChange('npp', input, this.getNPPs);
  onStreetInputChange = input => this.onInputChange('street', input, this.getStreets);

  goToSummary = () => this.props.history.push('/checkout/summary');

  onContinueClick = async () => {
    const commonPayload = {
      deliveryId: this.state.deliveryId,
      countryId: this.state.countryId,
      countryName: this.state.countries.find(country => country.id === this.state.countryId).text,
      cityId: this.state.cityId,
      cityName: this.state.cities.find(city => city.id === this.state.cityId).text
    };

    if (this.state.deliveryId === 0) {
      await this.validateNovaPohstaDeliveryFields();

      const { cityError, nppError } = this.state;
      if (cityError || nppError) {
        if (cityError) this.cityComponent.startErrorAnimation();
        if (nppError) this.pointComponent.startErrorAnimation();
        return;
      }

      const adress = {
        ...commonPayload,
        nppId: this.state.nppId,
        nppName: this.state.nppInput
      };
      this.props.actions.updateUserInfo(adress);
      sendAmplitudeData('deliveryInfoStepDone', adress);
      this.goToSummary();
      return;
    }
    await this.validateUkrposhtaDeliveryFields();

    const { cityError, streetError, houseError, aptError, postcodeError } = this.state;
    if (cityError || streetError || houseError || aptError || postcodeError) {
      if (cityError) this.cityComponent.startErrorAnimation();
      if (streetError) this.streetComponent.startErrorAnimation();
      if (houseError) this.houseInput.startErrorAnimation();
      if (aptError) this.aptInput.startErrorAnimation();
      if (postcodeError) this.postcodeInput.startErrorAnimation();
      return;
    }

    const adress = {
      ...commonPayload,
      streetId: this.state.streetId,
      streetName: this.state.streetInput,
      house: this.state.houseInput,
      apartment: this.state.aptInput,
      postcode: this.state.postcodeInput
    };
    this.props.actions.updateUserInfo(adress);
    sendAmplitudeData('deliveryInfoStepDone', adress);
    this.goToSummary();
  };

  renderDeliverySpecificFields = () => {
    if (this.state.deliveryId === 0) {
      return (
        <div>
          <SelectWithInput
            ref={component => (this.pointComponent = component)}
            onChange={this.onNPPChange}
            onInputChange={this.onNPPInputChange}
            value={this.state.nppInput}
            label='Отделение'
            filterByInputEnabled={true}
            errorVisible={this.state.nppError}
            errorText={this.state.nppErrorText}
            disabled={this.state.cityId === null}
            options={this.state.npps}
            choosenOptionId={this.state.nppId}
            placeholder='Выберите отделение'
            emptyListText='Пусто'
            pending={this.state.nppsPending}
          />
        </div>
      );
    }

    return (
      <div>
        <SelectWithInput
          ref={component => (this.streetComponent = component)}
          onChange={this.onStreetChange}
          onInputChange={this.onStreetInputChange}
          value={this.state.streetInput}
          label='Улица'
          errorVisible={this.state.streetError}
          errorText={this.state.streetErrorText}
          disabled={this.state.cityId === null}
          options={this.state.streets}
          choosenOptionId={this.state.streetId}
          placeholder='Выберите улицу'
          pending={this.state.streetsPending}
          emptyListText='Пусто'
        />
        <Input
          ref={input => (this.houseInput = input)}
          onChange={value => this.onInputChange('house', value)}
          value={this.state.houseInput}
          errorVisible={this.state.houseError}
          errorText={this.state.houseErrorText}
          label='Дом'
          onBlur={this.validateHouse}
          disabled={this.state.streetId === null}
        />
        <Input
          ref={input => (this.aptInput = input)}
          onChange={value => this.onInputChange('apt', value)}
          value={this.state.aptInput}
          errorVisible={this.state.aptError}
          errorText={this.state.aptErrorText}
          label='Квартира'
          onBlur={this.validateApt}
          disabled={this.state.streetId === null}
        />
        <Input
          ref={input => (this.postcodeInput = input)}
          onChange={value => this.onInputChange('postcode', value)}
          value={this.state.postcodeInput}
          errorVisible={this.state.postcodeError}
          errorText={this.state.postcodeErrorText}
          label='Почтовый индекс'
          onBlur={this.validatePostcode}
          disabled={this.state.streetId === null}
        />
      </div>
    );
  };

  render() {
    return (
      <Container>
        <Header>Доставка</Header>
        <Select
          onChange={this.setDeliveryId}
          label='Способ доставки'
          options={deliveryOptions}
          choosenOptionId={this.state.deliveryId}
          placeholder='Выберите способ доставки'
        />
        <Select
          onChange={this.setCountryId}
          label='Страна'
          options={this.state.countries}
          choosenOptionId={this.state.countryId}
          placeholder='Выберите страну'
        />
        <SelectWithInput
          ref={component => (this.cityComponent = component)}
          onChange={this.onCityChange}
          onInputChange={this.onCityInputChange}
          value={this.state.cityInput}
          label='Город'
          errorVisible={this.state.cityError}
          errorText={this.state.cityErrorText}
          options={this.state.cities}
          choosenOptionId={this.state.cityId}
          placeholder='Выберите город'
          emptyListText='Пусто'
          pending={this.state.citiesPending}
        />
        {this.renderDeliverySpecificFields()}
        <Button label='Продолжить' onClick={this.onContinueClick} textStyle={{ fontSize: '12px' }} />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  deliveryId: state.user.deliveryId,
  countryId: state.user.countryId,
  countryName: state.user.countryName,
  cityId: state.user.cityId,
  cityName: state.user.cityName,
  nppId: state.user.nppId,
  nppName: state.user.nppName,
  streetId: state.user.streetId,
  streetName: state.user.streetName,
  house: state.user.house,
  apartment: state.user.apartment,
  postcode: state.user.postcode
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ updateUserInfo }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Delivery)
);

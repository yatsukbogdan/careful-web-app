import React, { Component } from 'react';
import styled from 'styled-components';
import { COLORS } from '../../../../../config';
import { media } from '../../../../../utils';

const Container = styled.div`
  flex: 1;
  margin-bottom: 15px;
`

const Separator = styled.div`
  margin: 10px 0;
  height: 1px;
  background-color: ${ COLORS.VERY_LIGHT_GRAY };
`

const ItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const ItemLeftText = styled.p`
  font-size: 18px;
  ${ media.smallPhone`font-size: 16px;` };
`

const ItemRightText = styled.p`
  font-size: 18px;
  ${ media.smallPhone`font-size: 16px;` };
  font-weight: 500;
  text-align:right;
`

export default class SummaryList extends Component {
  render() {
    const { fields } = this.props;
    
    return (
      <Container>
        { fields.map((field, index) => (
          <div>
            {index !== 0 && <Separator />}
            <ItemContainer>
              <ItemLeftText>{ field.label }</ItemLeftText>
              <ItemRightText>{ field.value }</ItemRightText>
            </ItemContainer>
          </div>
        )) }
      </Container>
    )
  }
}

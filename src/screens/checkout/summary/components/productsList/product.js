import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { numberToCurrency, media } from '../../../../../utils';
import { COLORS } from '../../../../../config';

const Container = styled.div`
  display: flex;
`

const Image = styled.img`
  height: 80px;
  width: 80px;
  ${ media.smallPhone`height: 70px; width: 70px;` };
`

const NameContainer = styled.div`
  margin: 0 15px;
  display: flex;
  flex: 1;
`

const NameText = styled.p`
  font-size: 20px;
  ${ media.smallPhone`font-size: 16px;` };
`

const PriceContainer = styled.div`
  flex-direction: column;
  display: flex;
  align-items: flex-end;
`

const PriceText = styled.p`
  ${ props => props.discountPrice && css`text-decoration: line-through;`};
  font-size: ${ props => props.discountPrice ? '14px' : '20px'};
  ${ media.smallPhone`font-size: ${ props => props.discountPrice ? '12px' : '16px'};` };
`

const DiscountPriceText = styled.p`
  font-size: 20px;
  ${ media.smallPhone`font-size: 16px;` };
  color: ${ COLORS.MAIN_COLOR };
`

export default class SummaryProduct extends Component {
  render() {
    const { product } = this.props;

    return (
      <Container>
        <Image src={ product.album[0] }/>
        <NameContainer>
          <NameText>{ product.name } (x{ product.quantity })</NameText>
        </NameContainer>
        <PriceContainer>
          { product.discountPrice && <DiscountPriceText>{ numberToCurrency(product.discountPrice * product.quantity, product.currency) }</DiscountPriceText>}
          <PriceText discountPrice={ product.discountPrice }>{ numberToCurrency(product.price * product.quantity, product.currency) }</PriceText>
        </PriceContainer>
      </Container>
    )
  }
}

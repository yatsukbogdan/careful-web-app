import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import SummaryProduct from './product';
import { COLORS } from '../../../../../config';
import { numberToCurrency, media } from '../../../../../utils';

const NoProductsContainer = styled.div`
  height: 100px;
  align-items: center;
  justify-content: center;
  display: flex;
`

const NoProductsText = styled.p`
  opacity: 0.2;
`

const Separator = styled.div`
  margin: 10px 0;
  height: 1px;
  background-color: ${ COLORS.VERY_LIGHT_GRAY };
`

const TotalPriceContainer = styled.div`
  display: flex;
  align-items: flex-start;
`

const TotalPriceLabelContainer = styled.div`
  flex: 1;
`

const TotalPriceLabel = styled.p`
  font-size: 18px;
  ${ media.smallPhone`font-size: 16px;` };
`

const TotalDiscountPriceText = styled.p`
  font-size: 18px;
  ${ media.smallPhone`font-size: 16px;` };
  font-weight: 500;
  margin-right: 5px;
  color: ${ COLORS.MAIN_COLOR };
`

const TotalPriceText = styled.p`
  ${ props => props.discountPrice && css`text-decoration: line-through;`};
  font-size: ${ props => props.discountPrice ? '14px' : '18px'};
  font-weight: ${ props => props.discountPrice ? 'normal' : '500'};
  ${ media.smallPhone`font-size: ${ props => props.discountPrice ? '12px' : '16px'};` };
`

export default class SummaryProductsList extends Component {
  renderTotalPrice = () => {
    const { products } = this.props;
    if (products.length === 0) return false;

    const prices = products.reduce((_prices, product) => ({
      normal: _prices.normal + product.price * product.quantity,
      discount: product.discountPrice ? _prices.discount + product.discountPrice * product.quantity : _prices.discount + product.price * product.quantity
    }), {
      normal: 0,
      discount: 0
    });
    
    const { currency } = products[0];
    const discountPrice = prices.normal === prices.discount ? null : prices.discount;
    const price = prices.normal;

    return (
      <div>
        <Separator />
        <TotalPriceContainer>
          <TotalPriceLabelContainer>
            <TotalPriceLabel>Итого к оплате:</TotalPriceLabel>
          </TotalPriceLabelContainer>
          { discountPrice && <TotalDiscountPriceText>{ numberToCurrency(discountPrice, currency) }</TotalDiscountPriceText> }
          <TotalPriceText discountPrice={discountPrice}>{ numberToCurrency(price, currency) }</TotalPriceText>
        </TotalPriceContainer>
        <Separator />
      </div>
    )
  }

  render() {
    const { products } = this.props;
    if (products.length === 0) {
      return (
        <NoProductsContainer>
          <NoProductsText>Корзина пуста</NoProductsText>
        </NoProductsContainer>
      )
    }

    return (
      <div>
        { products.map((product, index) => (
          <div>
            {index !== 0 && <Separator />}
            <SummaryProduct product={ product }/>
          </div>
        )) }
        { this.renderTotalPrice() }
      </div>
    )
  }
}

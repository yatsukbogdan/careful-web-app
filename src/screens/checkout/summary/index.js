import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Base64 } from 'js-base64';
import { createHash } from 'crypto';
import { withRouter } from 'react-router-dom';
import Button from '../../../components/button';
import ProductsList from './components/productsList';
import SummaryList from './components/summaryList';
import { deliveryOptions, media } from '../../../utils';
import { LIQPAY_PUBLIC_KEY, LIQPAY_PRIVATE_KEY, API_URL, WEB_APP_URL } from '../../../config';
import API from '../../../api';
import { removeProductFromCart } from '../../../actions/cart';
import { sendAmplitudeData, setAmplitudeUserId } from '../../../amplitude';
import ErrorModal from '../../../components/errorModal';

const Container = styled.div`
  padding: 25px;
  ${media.smallPhone`padding: 18px;`};
`;

const Header = styled.p`
  letter-spacing: 2px;
  margin-bottom: 25px;
  font-size: 24px;
  ${media.smallPhone`
    font-size: 20px;
    margin-bottom: 15px;
  `};
`;

class Summary extends Component {
  state = {
    errorModalVisible: false,
    errorModalText: '',
    errorModalButtonText: '',
    pending: false
  };

  onContinueClick = async () => {
    this.setState({ pending: true });

    sendAmplitudeData('onPayClick');

    const customerData = (await API.addCustomer({
      firstName: this.props.user.name,
      lastName: this.props.user.surname,
      email: this.props.user.email,
      phone: this.props.user.phone
    })).data;

    if (!customerData.success) {
      this.setState({ pending: false });
      // TODO show popup
      sendAmplitudeData('paymentError', { message: customerData.message });
      alert(customerData.message);
      return;
    }

    const customerId = customerData.payload.id;
    setAmplitudeUserId(customerId);
    localStorage.setItem('customerId', customerId);
    const invoiceData = (await API.addInvoice({
      products: this.props.cartProducts,
      type: 1
    })).data;

    if (!invoiceData.success) {
      const { payload } = invoiceData;
      this.setState({ pending: false });
      if (payload.code === 400001) {
        this.showErrorModal(payload.productsIds, this.props.products.length > payload.productsIds.length);
        payload.productsIds.forEach(id => this.props.removeProductFromCart({ id }));
        sendAmplitudeData('paymentError', { message: invoiceData.message });
      }
      return;
    }

    const invoiceId = invoiceData.payload.id;
    const deliveryBody = {
      invoiceId,
      customerId,
      method: this.props.user.deliveryId,
      countryId: this.props.user.countryId,
      city: {
        name: this.props.user.cityName,
        id: this.props.user.cityId,
        source: this.props.user.deliveryId === 0 ? 'novaPoshta' : 'rozetka'
      },
      deliverySpecificData: {}
    };

    if (this.props.user.deliveryId === 0) {
      deliveryBody.deliverySpecificData.nppId = this.props.user.nppId;
      deliveryBody.deliverySpecificData.nppName = this.props.user.nppName;
    } else {
      deliveryBody.deliverySpecificData.streetId = this.props.user.streetId;
      deliveryBody.deliverySpecificData.streetName = this.props.user.streetName;
      deliveryBody.deliverySpecificData.house = this.props.user.house;
      deliveryBody.deliverySpecificData.apartment = this.props.user.apartment;
      deliveryBody.deliverySpecificData.postcode = this.props.user.postcode;
    }

    const deliveryData = (await API.addDelivery(deliveryBody)).data;

    if (!deliveryData.success) {
      this.setState({ pending: false });
      // TODO show popup
      sendAmplitudeData('paymentError', { message: deliveryData.message });
      alert(deliveryData.message);
      return;
    }
    this.setState({ pending: false });

    const price = this.props.products.reduce((sum, product) => {
      if (product.discountPrice) return sum + product.discountPrice * product.quantity;
      return sum + product.price * product.quantity;
    }, 0);
    const dataJson = {
      version: 3,
      public_key: LIQPAY_PUBLIC_KEY,
      private_key: LIQPAY_PRIVATE_KEY,
      action: 'pay',
      amount: price,
      currency: 'UAH',
      order_id: deliveryData.payload.id,
      result_url: `${WEB_APP_URL}/checkout/post-liqpay`,
      server_url: `${API_URL}/liqpay`
    };
    localStorage.setItem('invoiceId', invoiceId);

    const data = Base64.encode(JSON.stringify(dataJson));
    const signString = `${LIQPAY_PRIVATE_KEY}${data}${LIQPAY_PRIVATE_KEY}`;
    const shaSum = createHash('sha1');
    shaSum.update(signString);
    const signature = shaSum.digest('base64');

    const form = document.createElement('form');
    form.method = 'POST';
    form.action = 'https://www.liqpay.ua/api/3/checkout';
    form.acceptCharset = 'utf-8';

    const dataInput = document.createElement('input');
    dataInput.type = 'hidden';
    dataInput.name = 'data';
    dataInput.value = data;
    form.appendChild(dataInput);

    const signatureInput = document.createElement('input');
    signatureInput.type = 'hidden';
    signatureInput.name = 'signature';
    signatureInput.value = signature;

    form.appendChild(signatureInput);
    document.body.appendChild(form);
    form.submit();
  };

  showErrorModal = (productsIds, someProductsLeft) => {
    let errorText = 'К сожалению, в процессе оформления заказа закончился товар ';
    for (let i in productsIds) {
      const { name } = this.props.productsObject[productsIds[i]];
      if (i != 0) errorText += ', ';
      errorText += `"${name}"`;
    }
    errorText += '.';
    if (someProductsLeft) {
      errorText += ' Вы можете оплатить заказа без указаного товара.';
    }
    this.setState({
      errorModalText: errorText,
      errorModalVisible: true,
      errorModalButtonText: someProductsLeft ? 'Оплатить заказ' : 'Вернуться на главную'
    });
  };
  hideErrorModal = () => this.setState({ errorModalText: '', errorModalVisible: false, errorModalButtonText: '' });

  onErrorModalButtonClick = () => {
    this.hideErrorModal();
    if (this.props.products.length > 0) {
      setTimeout(() => this.props.showCartModal(), 200);
      return;
    }
    this.props.history.push('/');
  };

  getSummaryFields = () => {
    const { user } = this.props;
    const deliveryName = deliveryOptions.find(del => del.id === user.deliveryId).text;

    if (deliveryName === null) return [];
    const commonPayload = [
      {
        label: 'Способ доставки',
        value: deliveryName
      },
      {
        label: 'Страна',
        value: user.countryName
      },
      {
        label: 'Город',
        value: user.cityName
      }
    ];
    if (user.deliveryId === 0)
      return commonPayload.concat({
        label: 'Отделение',
        value: user.nppName
      });

    return commonPayload.concat([
      {
        label: 'Улица',
        value: user.streetName
      },
      {
        label: 'Дом',
        value: user.house
      },
      {
        label: 'Квартира',
        value: user.apartment
      },
      {
        label: 'Почтовый индекс',
        value: user.postcode
      }
    ]);
  };

  renderButton = () => {
    if (this.props.products.length > 0) {
      return (
        <Button
          pending={this.state.pending}
          label='Перейти к оплате'
          onClick={this.onContinueClick}
          textStyle={{ fontSize: '12px' }}
        />
      );
    }
    return (
      <Button
        pending={this.state.pending}
        label='Вернуться на главную'
        onClick={() => this.props.history.push('/')}
        textStyle={{ fontSize: '12px' }}
      />
    );
  };

  render() {
    return (
      <Container>
        <Header>Ваш заказ</Header>
        <ProductsList products={this.props.products} />
        <SummaryList fields={this.getSummaryFields()} />
        {this.renderButton()}
        <ErrorModal
          text={this.state.errorModalText}
          visible={this.state.errorModalVisible}
          close={this.hideErrorModal}
          onButtonClick={this.onErrorModalButtonClick}
          buttonText={this.state.errorModalButtonText}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  const cartProducts = state.cart.products.filter(product => state.products[product.id] != null);

  return {
    cartProducts,
    productsObject: state.products,
    products: cartProducts.map(product => ({
      ...state.products[product.id],
      quantity: product.quantity
    })),
    user: state.user
  };
};

const mapDispatchToProps = {
  removeProductFromCart
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Summary)
);

import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spinner } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { withRouter } from 'react-router';
import { MdDone, MdClear } from 'react-icons/md';
import { COLORS } from '../../../config';
import { sendAmplitudeData } from '../../../amplitude';
import API from '../../../api';
import { clearCart } from '../../../actions/cart';

const Container = styled.div`
  padding: 25px;
  display: flex;
  flex-direction: column;
`;

const ActivityContainer = styled.div`
  height: 500px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const MessageContainer = styled.div`
  flex-direction: column;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 50px;
`;

const Text = styled.p`
  margin-bottom: 20px;
  text-align: center;
  font-size: 20px;
`;

const BoldText = styled.span`
  font-weight: bold;
`;

const MediumText = styled.p`
  margin-bottom: 10px;
  text-align: center;
  font-size: 16px;
`;

const CheckMarkContainer = styled.div`
  margin-bottom: 20px;
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 40px;
`;

const ButtonText = styled.p`
  text-align: center;
  font-size: ${props => (props.small ? '14px' : '18px')};
`;

class PostLiqpay extends Component {
  state = {
    success: false,
    pending: true,
    errorText: ''
  };

  componentDidMount() {
    const { invoiceId } = localStorage;
    API.checkInvoiceStatus(invoiceId).then(({ data }) => {
      if (data.success) return this.onSuccess();
      this.onError(data.message);
    });
  }

  onSuccess = () => {
    sendAmplitudeData('paymentSuccesfull');
    this.props.actions.clearCart();
    this.setState({
      success: true,
      pending: false
    });
  };

  onError = errorText => {
    sendAmplitudeData('paymentFailed', { message: errorText });
    this.setState({
      success: false,
      pending: false,
      errorText
    });
  };

  toMainPage = () => this.props.history.push('/');
  toOrder = () => this.props.history.push('/checkout/summary');

  render() {
    if (this.state.pending) {
      return (
        <ActivityContainer>
          <Spinner size={30} color={COLORS.MAIN_COLOR} />
        </ActivityContainer>
      );
    }

    if (this.state.success) {
      return (
        <Container>
          <MessageContainer>
            <CheckMarkContainer>
              <MdDone size={170} color={COLORS.MAIN_COLOR} />
            </CheckMarkContainer>
            <Text>Оплата прошла успешно</Text>
            <MediumText>
              на почту
              <BoldText> {this.props.email} </BoldText>
              будет отправлено письмо с деталями вашего заказа
            </MediumText>
          </MessageContainer>
          <ButtonContainer onClick={this.toMainPage}>
            <ButtonText>Вернутся на главную</ButtonText>
          </ButtonContainer>
        </Container>
      );
    }

    return (
      <Container>
        <MessageContainer>
          <CheckMarkContainer>
            <MdClear size={170} color={COLORS.LIGHT_RED} />
          </CheckMarkContainer>
          <Text>Что-то пошло не так</Text>
          <MediumText>
            Ваш заказ пока не обработан. Попробуйте обновить страничку через несколько минут. Если заказ не будет
            обработан в течение 10 минут, напишите на почту carefulstore.office@gmail.com.
          </MediumText>
          <MediumText>Спасибо за понимание!</MediumText>
          <MediumText>Если Вы отменили оплату, проигнорируйте это сообщение.</MediumText>
        </MessageContainer>
        <ButtonContainer onClick={this.toOrder}>
          <ButtonText>Вернутся к заказу</ButtonText>
        </ButtonContainer>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  email: state.user.email
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      clearCart
    },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PostLiqpay)
);

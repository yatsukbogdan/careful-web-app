import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import Input from '../../../components/input';
import Button from '../../../components/button';
import { sendAmplitudeData, setAmplitudeUserProperties } from '../../../amplitude';
import { updateUserInfo } from '../../../actions/user';
import { nameValidation, emailValidation, phoneValidation, surnameValidation } from '../../../utils';

const Container = styled.div`
  padding: 25px;
`;

const Header = styled.p`
  letter-spacing: 2px;
  margin-bottom: 25px;
  font-size: 24px;
`;

class PersonalInfo extends Component {
  state = {
    nameInput: this.props.name ? this.props.name : '',
    nameError: false,
    nameErrorText: '',

    surnameInput: this.props.surname ? this.props.surname : '',
    surnameError: false,
    surnameErrorText: '',

    emailInput: this.props.email ? this.props.email : '',
    emailError: false,
    emailErrorText: '',

    phoneInput: this.props.phone ? this.props.phone : '',
    phoneError: false,
    phoneErrorText: ''
  };

  onFieldChange = (value, field) =>
    this.setState({
      [`${field}Input`]: value,
      [`${field}Error`]: false,
      [`${field}ErrorText`]: ''
    });

  validateField = async field => {
    let validationFunction;

    switch (field) {
      case 'name':
        validationFunction = nameValidation;
        break;
      case 'surname':
        validationFunction = surnameValidation;
        break;
      case 'email':
        validationFunction = emailValidation;
        break;
      case 'phone':
        validationFunction = phoneValidation;
        break;
      default:
        validationFunction = nameValidation;
        break;
    }

    const validationObject = validationFunction(this.state[`${field}Input`]);

    if (validationObject.success) return;

    await this.setState({
      [`${field}Error`]: true,
      [`${field}ErrorText`]: validationObject.error
    });
  };

  validateAllFields = async () => {
    await Promise.all([
      this.validateField('name'),
      this.validateField('surname'),
      this.validateField('email'),
      this.validateField('phone')
    ]);
  };

  onContinueClick = async () => {
    await this.validateAllFields();
    const { nameError, surnameError, emailError, phoneError } = this.state;

    if (nameError || surnameError || emailError || phoneError) {
      if (nameError) this.nameInput.startErrorAnimation();
      if (surnameError) this.surnameInput.startErrorAnimation();
      if (emailError) this.emailInput.startErrorAnimation();
      if (phoneError) this.phoneInput.startErrorAnimation();
      return;
    }

    const { nameInput, surnameInput, emailInput, phoneInput } = this.state;

    const userData = {
      name: nameInput,
      surname: surnameInput,
      email: emailInput,
      phone: phoneInput
    };
    this.props.actions.updateUserInfo(userData);
    sendAmplitudeData('personalInfoStepDone');
    setAmplitudeUserProperties(userData);
    this.props.history.push('/checkout/delivery');
  };

  render() {
    return (
      <Container>
        <Header>Личные данные</Header>
        <Input
          value={this.state.nameInput}
          label='Имя'
          onChange={value => this.onFieldChange(value, 'name')}
          onBlur={() => this.validateField('name')}
          ref={input => (this.nameInput = input)}
          onSubmit={() => this.surnameInput.input.focus()}
          errorVisible={this.state.nameError}
          errorText={this.state.nameErrorText}
        />
        <Input
          value={this.state.surnameInput}
          label='Фамилия'
          ref={input => (this.surnameInput = input)}
          onSubmit={() => this.emailInput.input.focus()}
          onChange={value => this.onFieldChange(value, 'surname')}
          onBlur={() => this.validateField('surname')}
          errorVisible={this.state.surnameError}
          errorText={this.state.surnameErrorText}
        />
        <Input
          value={this.state.emailInput}
          label='Электронная почта'
          onSubmit={() => this.phoneInput.input.focus()}
          ref={input => (this.emailInput = input)}
          onChange={value => this.onFieldChange(value, 'email')}
          onBlur={() => this.validateField('email')}
          type='email'
          errorVisible={this.state.emailError}
          errorText={this.state.emailErrorText}
        />
        <Input
          ref={input => (this.phoneInput = input)}
          value={this.state.phoneInput}
          label='Номер телефона'
          type='tel'
          onChange={value => this.onFieldChange(value, 'phone')}
          onBlur={() => this.validateField('phone')}
          errorVisible={this.state.phoneError}
          errorText={this.state.phoneErrorText}
        />
        <Button label='Продолжить' onClick={this.onContinueClick} textStyle={{ fontSize: '12px' }} />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  name: state.user.name,
  surname: state.user.surname,
  email: state.user.email,
  phone: state.user.phone
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ updateUserInfo }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PersonalInfo)
);

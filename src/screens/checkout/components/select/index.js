import styled, { css, keyframes } from 'styled-components';
import React, { Component } from 'react';
import { IoIosArrowDown, IoIosCheckmark } from 'react-icons/io';
import { COLORS } from '../../../../config';

const Container = styled.div`
  position: relative;
  margin-bottom: 10px;
`

const TextContainer = styled.div`
  box-sizing: border-box;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 5px 0 10px;
  margin-bottom: 30px;
  border: 1px ${ COLORS.VERY_LIGHT_GRAY } solid;
  ${ props => props.error && css`border: 1px ${ COLORS.LIGHT_RED } solid; margin-bottom: 0;`};
`

const ChoosenOptionText = styled.p`
  font-size: 18px;
  font-weight: 100;
`

const ANIMATION_DURATION_MILLISECONDS = 200;
const shakeAnimation = keyframes`
  20%, 80% {
    transform: translate3d(-2px, 0, 0);
  }
  40%, 60% {
    transform: translate3d(2px, 0, 0);
  }
`

const Label = styled.p`
  color: ${ COLORS.GRAY };
  ${ props => props.error && css`color: ${ COLORS.LIGHT_RED };`};
  ${ props => props.errorAnimation && css`animation: ${ shakeAnimation } ${ ANIMATION_DURATION_MILLISECONDS }ms;` }
  font-weight: 400;
  margin-bottom: 5px;
  font-size: 16px;
`

const ArrowContainer = styled.div`
  transition: 0.3s;
  transform: ${ props => props.down ? 'rotate(0deg)' : 'rotate(180deg)' };
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
`

const SelectContainer = styled.div`
  overflow-y: scroll;
  z-index: 2;
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  background-color: white;
  top: 63px;
  max-height: 200px;
  border: 1px ${ COLORS.VERY_LIGHT_GRAY } solid;
`

const Separator = styled.div`
  height: 1px;
  margin: 0 15px;
  background-color: ${ COLORS.VERY_LIGHT_GRAY };
`

const OptionContainer = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 15px;
`

const OptionText = styled.p`
  font-size: 16px;
  font-weight: 100;
`

const ErrorContainer = styled.div`
  display: flex;
  height: 20px;
  align-items: center;
`

const ErrorText = styled.p`
  text-transform: uppercase;
  color: ${ COLORS.LIGHT_RED };
  font-size: 10px;
`

export default class Select extends Component {
  state = {
    errorAnimation: false,
    opened: false
  }

  startErrorAnimation = () => {
    this.setState({ errorAnimation: true })
    setTimeout(() => this.setState({errorAnimation: false}), ANIMATION_DURATION_MILLISECONDS)
  }

  toggleSelect = () => {
    if (this.state.opened) {
      this.close();
      return;
    }

    this.open();
  }

  open = () => this.setState({ opened: true })
  close = () => this.setState({ opened: false })

  onChange = id => {
    this.close();
    this.props.onChange(id);
  }

  renderSelect = () => {
    if (!this.state.opened) return;
    const { options, choosenOptionId } = this.props;

    return (
      <SelectContainer>
        { options.map((option, index) => (
          <div key={ option.id }>
            { index !== 0 && <Separator />}
            <OptionContainer onClick={ () => this.onChange(option.id) }>
              <OptionText>{ option.text }</OptionText>
              { option.id === choosenOptionId && <IoIosCheckmark size={ 30 } color={ COLORS.MAIN_COLOR }/>}
            </OptionContainer>
          </div>
        )) }
      </SelectContainer>
    );
  }

  renderText = () => {
    const { choosenOptionId, options } = this.props;

    if (choosenOptionId !== null) {
      const choosenOption = options.find(option => option.id === choosenOptionId);
      let text = choosenOption ? choosenOption.text : 'Invalid option';

      return <ChoosenOptionText > { text } </ChoosenOptionText>;
    }

    return <ChoosenOptionText style={{ color: COLORS.LIGHT_GRAY }}>{ this.props.placeholder }</ChoosenOptionText>;
  }

  render() {
    const { label, errorText, errorVisible } = this.props;
    const { opened } = this.state;

    return (
      <Container>
        <Label
          errorAnimation={ this.state.errorAnimation }
          error={ errorVisible }
        >
          { label }
        </Label>
        <TextContainer onClick={ this.toggleSelect } error={ errorVisible }>
          { this.renderText() }
          <ArrowContainer onClick={ this.toggleSelect } down={ !opened }>
            <IoIosArrowDown
              size={ 20 }
              color={ COLORS.MAIN_COLOR }
            />
          </ArrowContainer>
        </TextContainer>
        { errorVisible &&
          <ErrorContainer>
            <ErrorText>{ errorText }</ErrorText>
          </ErrorContainer>
        }
        { this.renderSelect() }
      </Container>
    )
  }
}
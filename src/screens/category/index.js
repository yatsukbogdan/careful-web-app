import React, { Component } from 'react';
import ProductsList from '../../components/productsList';
import API from '../../api';
import { addProducts } from '../../actions/products';
import Footer from '../../components/footer';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { media } from '../../utils';

const Container = styled.div`
  ${media.retina`width: 30%;`};
  ${media.tablet`width: 50%;`};
  ${media.phone`width: 100%;`};
  transform: translate(-50%, 0);
  margin-left: 50%;
`;

class CategoryScreen extends Component {
  state = {
    error: null,
    loading: false
  };

  async componentDidMount() {
    const { id } = this.props.match.params;

    this.setState({ loading: true });
    const { data } = await API.getProductsList(id);
    this.setState({ loading: false });
    if (!data.success) return this.setState({ error: data.message });
    this.props.actions.addProducts({ products: data.payload.products });
  }

  render() {
    const { products, categoryName } = this.props;

    return (
      <Container>
        <ProductsList products={products} label={categoryName} />
        <Footer />
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params;
  const { products, categories } = state;
  const productsIds = Object.keys(products);
  const _products = productsIds.filter(prodId => products[prodId].categoryId === id).map(id => products[id]);

  return {
    products: _products,
    categoryName: categories[id] ? categories[id].name : null
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ addProducts }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CategoryScreen)
);

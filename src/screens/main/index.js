import React, { Component } from 'react';
import ProductsList from '../../components/productsList';
import API from '../../api';
import { addProducts } from '../../actions/products';
import Footer from '../../components/footer';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { media } from '../../utils';

const Container = styled.div`
  ${media.retina`width: 30%;`};
  ${media.tablet`width: 50%;`};
  ${media.phone`width: 100%;`};
  transform: translate(-50%, 0);
  margin-left: 50%;
`;

class MainScreen extends Component {
  state = {
    error: null,
    loading: false
  };

  async componentDidMount() {
    this.setState({ loading: true });
    const { data } = await API.getProductsList();
    this.setState({ loading: false });
    if (!data.success) return this.setState({ error: data.message });
    this.props.actions.addProducts({ products: data.payload.products });
  }

  render() {
    const { products } = this.props;

    return (
      <Container>
        <ProductsList products={products} />
        <Footer />
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  productsObject: state.products,
  products: Object.keys(state.products).map(id => state.products[id])
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ addProducts }, dispatch)
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MainScreen)
);

import React, { Component } from 'react';
import styled from 'styled-components';
import Footer from '../../components/footer';

const Container = styled.div`
  min-height: calc(100vh - 60px);
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

const InnerContainer = styled.div`
  padding: 20px;
`;

const BigText = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 20px;
`;

const Text = styled.p`
  margin-bottom: 15px;
`;

export default class AboutUs extends Component {
  state = {
    error: null,
    loading: false
  };

  render() {
    return (
      <Container>
        <InnerContainer>
          <BigText>О нас</BigText>
          <Text>
            Жила-была девушка. Однажды на просторах интернета она наткнулась на информацию о том, что её Планета
            погибает. Начала изучать, копать глубже, и поняла, что проблема реальна. Поняла, что решить эту проблему
            возможно, если жители её Планеты сплотятся. Поняла, что начинать нужно с себя. Поняла, что нужно заботиться
            о её Планете.
          </Text>
          <Text>
            Девушка пересмотрела свои привычки, нашла экологичные альтернативы повседневным вещам, которые убивают её
            Планету. Этот поиск был тернистым и сложным. В её Стране забота о Планете считалась скорее странностью, чем
            хорошим делом. Поэтому девушка решила, что хочет облегчить этот путь для других. Что хочет создать место,
            где её единомышленники смогут найти качественные экологичные альтернативы. Что это место должно быть
            комфортным, а поиск - приятным.
          </Text>
          <Text>
            Так и появился Careful Store - самый эстетичный магазин экотоваров в Украине. Дело было в 2018 году, а
            девушку звали Женя.
          </Text>
          <Text>Careful Store создан для людей, которые заботятся. For people who care.</Text>
        </InnerContainer>
        <Footer />
      </Container>
    );
  }
}

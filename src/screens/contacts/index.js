import React, { Component } from 'react';
import styled from 'styled-components';
import Footer from '../../components/footer';

const Container = styled.div`
  min-height: calc(100vh - 60px);
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

const InnerContainer = styled.div`
  padding: 20px;
`;

const BigText = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 20px;
`;

export default class ContactsScreen extends Component {
  state = {
    error: null,
    loading: false
  };

  render() {
    return (
      <Container>
        <InnerContainer>
          <BigText>Контакты</BigText>
          <p style={{ marginBottom: '15px' }}>Звоните: +380502752772</p>
          <p style={{ marginBottom: '15px' }}>
            Пишите: <span style={{ fontWeight: '500' }}>carefulstore.office@gmail.com</span>
          </p>
          <p>Адрес: г. Киев, пер. Ковальский, д. 5</p>
        </InnerContainer>
        <Footer />
      </Container>
    );
  }
}

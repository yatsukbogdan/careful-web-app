import React, { Component } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import { numberToCurrency } from '../../utils';
import { sendAmplitudeData } from '../../amplitude';
import { COLORS } from '../../config';

const Container = styled.div`
  filter: ${props => (props.balance > 0 ? 'none' : 'grayscale(100%)')};
`;

const Image = styled.img`
  object-fit: cover;
`;

const Text = styled.p`
  text-align: center;
  letter-spacing: 1px;
  color: black;
`;

const ProductName = styled(Text)`
  font-size: 16px;
  margin-top: 10px;
`;

const ProductDiscountPrice = styled(Text)`
  margin-top: 5px;
  font-size: 16px;
  color: ${COLORS.MAIN_COLOR};
`;

const ProductPrice = styled(Text)`
  text-decoration: ${props => (props.discountPrice !== null ? 'line-through' : 'none')};
  font-size: ${props => (props.discountPrice !== null ? '12px' : '16px')};
`;

const OutOfStockText = styled(Text)`
  margin-top: 5px;
  font-size: 12px;
  color: ${COLORS.GRAY};
`;

export default class Product extends Component {
  render() {
    const { product } = this.props;

    return (
      <Link
        onClick={() => sendAmplitudeData('onProductClick', { id: product.id, name: product.name })}
        style={{ textDecoration: 'none' }}
        to={{ pathname: `/product/${product.id}`, state: { product } }}
      >
        <Container balance={product.balance}>
          <Slider arrows={false} infinite autoplay speed={500} slidesToShow={1} slidesToScroll={1}>
            {product.album.map(imageUrl => (
              <Image src={imageUrl} />
            ))}
          </Slider>
        </Container>
        <ProductName>{product.name}</ProductName>
        {product.balance > 0 ? (
          <div>
            {product.discountPrice && (
              <ProductDiscountPrice>{numberToCurrency(product.discountPrice, product.currency)}</ProductDiscountPrice>
            )}
            <ProductPrice discountPrice={product.discountPrice}>
              {numberToCurrency(product.price, product.currency)}
            </ProductPrice>
          </div>
        ) : (
          <OutOfStockText>Нет в наличии</OutOfStockText>
        )}
      </Link>
    );
  }
}

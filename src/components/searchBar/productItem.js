import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { COLORS } from '../../config';
import { numberToCurrency } from '../../utils';

const Container = styled.div`
  ${ props => props.outOfStock && css`filter: grayscale(100%);` };
  display: flex;
  justify-content: space-between;
  height: 50px;
  padding: 5px 15px;
`

const ImageAndNameContainer = styled.div`
  display: flex;
`

const Image = styled.img`
  height: 50px;
  width: 50px;
  margin-right: 15px;
`

const Name = styled.p`
  font-size: 16px;
`

const PriceContainer = styled.div`
  flex-direction: column;
  display: flex;
  align-items: flex-end;
`

const PriceText = styled.p`
  ${ props => props.discountPrice && css`text-decoration: line-through;`};
  font-size: ${ props => props.discountPrice ? '12px' : '16px'};
`

const DiscountPriceText = styled.p`
  font-size: 16px;
  color: ${ COLORS.MAIN_COLOR };
`

const OutOfStock = styled.p`
  font-size: 12px;
  color: ${ COLORS.LIGHT_GRAY };
`

export default class ProductItem extends Component {
  render() {
    const { product } = this.props;

    return (
      <Container onClick={ this.props.onClick } outOfStock={ product.balance === 0 }>
        <ImageAndNameContainer>
          <Image src={ product.album[0] }/>
          <Name>{ product.name }</Name>
        </ImageAndNameContainer>
        { product.balance > 0 ? (
          <PriceContainer>
            { product.discountPrice && <DiscountPriceText>{ numberToCurrency(product.discountPrice, product.currency) }</DiscountPriceText> }
            <PriceText discountPrice={product.discountPrice}>{ numberToCurrency(product.price, product.currency) }</PriceText>
          </PriceContainer>
        ) : <OutOfStock>Нет в наличии</OutOfStock>}
      </Container>
    )
  }
}
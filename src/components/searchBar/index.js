import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { IoIosArrowDropup, IoIosSearch } from 'react-icons/io';
import { sendAmplitudeData } from '../../amplitude';
import { COLORS } from '../../config';
import ProductItem from './productItem';

const OuterContainer = styled.div`
  z-index: ${props => (props.visible ? 300 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  display: flex;
  transition: 0.4s;
  top: 0;
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
`;

const SearchInputContainer = styled.div`
  position: absolute;
  left: 0;
  z-index: 11;
  height: 60px;
  padding: 0 15px;
  box-sizing: border-box;
  background-color: white;
  width: 100%;
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${COLORS.VERY_LIGHT_GRAY};
  justify-content: space-between;
  transition: transform 0.4s ease-out;
  ${props =>
    props.visible
      ? css`
          transform: translate(0, 0);
        `
      : css`
          transform: translate(0, -60px);
        `};
`;

const Input = styled.input`
  font-size: 15px;
  color: black;
  font-family: 'TT Norms';
  margin: 0 15px;
  width: 100%;
  padding: 5px 0;
  border-bottom: 1px ${COLORS.DARK_GREY} solid !important;
  outline: none;
`;

const ListContainer = styled.div`
  background-color: white;
  padding-top: 60px;
  height: 100vh;
  width: 100%;
  overflow-y: scroll;
  transition: transform 0.4s ease-out;
  ${props =>
    props.visible
      ? css`
          transform: translate(0, 0);
        `
      : css`
          transform: translate(0, -120vh);
        `};
  ${props =>
    props.empty &&
    css`
      display: flex;
      align-items: center;
      justify-content: center;
      height: 200px;
    `};
`;

const Separator = styled.div`
  height: 1px;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const TextEmpty = styled.div`
  font-size: 16px;
  color: ${COLORS.LIGHT_GRAY};
`;

class SearchBar extends Component {
  state = {
    input: '',
    listVisible: false
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.visible && nextProps.visible) this.input.focus();
    if (this.props.visible && !nextProps.visible) setTimeout(() => this.setState({ input: '' }), 400);
  }

  onInputChange = evt => this.setState({ input: evt.target.value });

  goToProduct = id => {
    const productName = this.props.products[id] ? this.props.products[id].name : 'unknown';
    sendAmplitudeData('onProductClick', { id, name: productName });
    this.props.close();
    this.props.history.push(`/product/${id}`);
  };

  getFilteredProductsIds = () => {
    const { products } = this.props;
    const productsIds = Object.keys(products);
    const input = this.state.input.trim().toLowerCase();
    return input.length > 0
      ? productsIds.filter(id => products[id].name.toLowerCase().indexOf(input) !== -1)
      : productsIds;
  };

  onDismiss = () => {
    if (this.state.input !== '') {
      sendAmplitudeData('searchBarInput', { input: this.state.input });
    }
    this.props.close();
  };

  render() {
    const { visible, products } = this.props;
    const productsIds = this.getFilteredProductsIds();

    return (
      <OuterContainer visible={visible}>
        <SearchInputContainer visible={visible}>
          <IoIosSearch size={30} />
          <Input
            ref={instance => (this.input = instance)}
            type='text'
            value={this.state.input}
            onChange={this.onInputChange}
          />
          <IoIosArrowDropup onClick={this.onDismiss} size={30} />
        </SearchInputContainer>
        <ListContainer empty={productsIds.length === 0} visible={visible}>
          {productsIds.length > 0 ? (
            productsIds.map((id, index) => (
              <div>
                {index !== 0 && <Separator />}
                <ProductItem onClick={() => this.goToProduct(id)} product={products[id]} />
              </div>
            ))
          ) : (
            <TextEmpty>Пусто</TextEmpty>
          )}
        </ListContainer>
      </OuterContainer>
    );
  }
}

const mapStateToProps = state => ({
  products: state.products
});

export default withRouter(connect(mapStateToProps)(SearchBar));

import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { IoIosSearch, IoIosMenu, IoIosCart } from 'react-icons/io';
import { COLORS } from '../../config';
import { sendAmplitudeData, getObjectFromCartProductsArray } from '../../amplitude';

const Container = styled.div`
  position: fixed;
  width: 100%;
  box-sizing: border-box;
  top: 0;
  z-index: 10;
  height: 60px;
  background-color: white;
  display: flex;
  padding: 0 15px;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px ${COLORS.VERY_LIGHT_GRAY} solid;
`;

const LeftButtonContainer = styled.div`
  display: flex;
  flex: 1;
`;

const TextContainer = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
`;

const Text = styled.p`
  font-family: 'Source Serif Pro';
  letter-spacing: 5px;
  font-size: 25px;
`;

const RightButtonContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
`;

const CartButtonContainer = styled.div`
  position: relative;
  margin-right: 10px;
`;

const CartCounter = styled.div`
  position: absolute;
  top: -5px;
  right: -5px;
  width: 12px;
  height: 12px;
  border-radius: 7px;
  background-color: white;
  border: 1px black solid;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const CartCounterText = styled.p`
  font-size: 10px;
`;

class Header extends Component {
  state = {
    searchInputValue: '',
    searchInputVisible: false
  };

  showSearchInput = () => this.setState({ searchInputVisible: true });
  hideSearchInput = () => this.setState({ searchInputVisible: false });

  handleSearchInputChange = event => this.setState({ searchInputValue: event.target.value });
  toMainPage = () => this.props.history.push('/');

  onCartClick = () => {
    sendAmplitudeData('onCartClick', getObjectFromCartProductsArray(this.props.cartProducts));
    this.props.onCartPress();
  };

  render() {
    return (
      <Container>
        <LeftButtonContainer onClick={this.props.onMenuPress}>
          <IoIosMenu size={20} />
        </LeftButtonContainer>
        <TextContainer onClick={this.toMainPage}>
          <Text>Careful</Text>
        </TextContainer>
        <RightButtonContainer>
          <CartButtonContainer onClick={this.onCartClick}>
            {this.props.cartProducts.length > 0 && (
              <CartCounter>
                <CartCounterText>{this.props.cartProducts.length}</CartCounterText>
              </CartCounter>
            )}
            <IoIosCart size={20} />
          </CartButtonContainer>
          <IoIosSearch onClick={this.props.onLoopPress} size={20} />
        </RightButtonContainer>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  cartProducts: state.cart.products
    .filter(product => state.products[product.id] != null)
    .map(product => ({
      ...product,
      name: state.products[product.id] ? state.products[product.id].name : 'unknown'
    }))
});

export default withRouter(connect(mapStateToProps)(Header));

import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { numberToCurrency, media } from '../../../../utils';
import { IoIosClose } from 'react-icons/io';
import QuantityController from './quantityController';
import { COLORS } from '../../../../config';

const Container = styled.div`
  display: flex;
  justify-content: center;
`

const Image = styled.img`
  height: 80px;
  width: 80px;
  margin-right: 15px;
  ${ media.smallPhone`height: 60px; width: 60px;` };
`

const NameCountContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
`

const QuantityCloseContainer = styled.div`
  display: flex;
  justify-content: flex-start;
`

const IoIosCloseContainer = styled.div`
  height: 30px;
  width: 30px;
  align-items: center;
  justify-content: center;
  display: flex;
  margin-left: 5px;
  ${ media.smallPhone`height: 24px; width: 24px;` };
`

const NameText = styled.p`
  font-size: 16px;
  margin-bottom: 8px;
`

// const BalanceText = styled.p`
//   font-size: 13px;
//   color: ${ COLORS.DARK_GREY };
// `

const PriceContainer = styled.div`
  flex-direction: column;
  display: flex;
  align-items: flex-end;
  justify-content: center;
`

const PriceText = styled.p`
  ${ props => props.discountPrice && css`text-decoration: line-through;`};
  font-size: ${ props => props.discountPrice ? '12px' : '16px'};
`

const DiscountPriceText = styled.p`
  font-size: 16px;
  color: ${ COLORS.MAIN_COLOR };
`

export default class CartProduct extends Component {
  render() {
    const { product } = this.props;

    return (
      <Container>
        <Image src={ product.album[0] }/>
        <NameCountContainer>
          <NameText>{ product.name }</NameText>
          <QuantityCloseContainer>
            <QuantityController
              balance={ product.balance }
              quantity={ product.quantity }
              onMinusPress={ this.props.onMinusPress }
              onPlusPress={ this.props.onPlusPress }
            />
            <IoIosCloseContainer>
              <IoIosClose
                onClick={ this.props.onCrossPress }
                size={ 20 }
                color={ COLORS.LIGHT_GRAY }
              />
            </IoIosCloseContainer>
          </QuantityCloseContainer>
          {/* <BalanceText>Доступно: { product.balance }</BalanceText> */}
        </NameCountContainer>
        <PriceContainer>
          { product.discountPrice && <DiscountPriceText>{ numberToCurrency(product.discountPrice * product.quantity, product.currency) }</DiscountPriceText>}
          <PriceText discountPrice={ product.discountPrice }>{ numberToCurrency(product.price * product.quantity, product.currency) }</PriceText>
        </PriceContainer>
      </Container>
    )
  }
}

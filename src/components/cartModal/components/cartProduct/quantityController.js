import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import { COLORS } from '../../../../config';
import { media } from '../../../../utils';

const Container = styled.div`
  display: flex;
`;

const Button = styled.div`
  ${props =>
    props.disabled &&
    css`
      opacity: 0.5;
      filter: grayscale(100%);
    `};
  height: 26px;
  width: 26px;
  ${media.smallPhone`height: 20px; width: 20px;`}
  border: 2px ${COLORS.MAIN_COLOR} solid;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const QuantityContainer = styled.div`
  height: 30px;
  width: 30px;
  ${media.smallPhone`height: 24px;`}
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Text = styled.p`
  font-size: 16px;
`;

export default class QuantityController extends Component {
  onMinusPress = () => {
    if (this.props.quantity === 1) return;
    this.props.onMinusPress();
  };

  onPlusPress = () => {
    if (this.props.quantity === this.props.balance) return;
    this.props.onPlusPress();
  };

  render() {
    const { balance, quantity } = this.props;

    return (
      <Container>
        <Button disabled={quantity === 1} onClick={this.onMinusPress}>
          <Text>-</Text>
        </Button>
        <QuantityContainer>{quantity}</QuantityContainer>
        <Button disabled={quantity === balance} onClick={this.onPlusPress}>
          <Text>+</Text>
        </Button>
      </Container>
    );
  }
}

import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import CartProduct from './components/cartProduct';
import { COLORS } from '../../config';
import { sendAmplitudeData, getObjectFromCartProductsArray } from '../../amplitude';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { incrementProductQuantity, decrementProductQuantity, removeProductFromCart } from '../../actions/cart';
import { IoIosClose } from 'react-icons/io';
import Button from '../button';
import { numberToCurrency } from '../../utils';

const OuterContainer = styled.div`
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  z-index: ${props => (props.visible ? 200 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  align-items: center;
  justify-content: center;
  display: flex;
  transition: 0.4s;
  top: 0;
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
`;

const InnerContainer = styled.div`
  box-sizing: border-box;
  padding: 20px;
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: white;
  height: 70%;
  width: 90%;
  transition: 0.4s;
  transform: ${props => (props.visible ? 'translate(0, 0)' : 'translate(0, 100vh)')};
`;

const CloseIconContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  height: 40px;
  width: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Header = styled.p`
  font-size: 24px;
  margin-bottom: 20px;
`;

const ProductsContainer = styled.div`
  overflow: scroll;
  flex: 1;
`;

const NoProductsContainer = styled.div`
  flex: 1;
  align-items: center;
  justify-content: center;
  display: flex;
`;

const NoProductsText = styled.p`
  opacity: 0.2;
`;

const Separator = styled.div`
  margin: 10px 0;
  height: 1px;
  background-color: ${COLORS.VERY_LIGHT_GRAY};
`;

const ContinueButtonContainer = styled.div`
  height: 30px;
  display: flex;
  align-items: flex-end;
  justify-content: center;
`;

const ContinueButtonText = styled.div`
  font-size: 12px;
  color: ${COLORS.MAIN_COLOR};
  letter-spacing: 4px;
  text-transform: uppercase;
`;

const TotalPriceContainer = styled.div`
  height: 30px;
  display: flex;
  align-items: flex-start;
`;

const TotalPriceLabelContainer = styled.div`
  flex: 1;
`;

const TotalPriceLabel = styled.p`
  font-size: 16px;
`;

const TotalDiscountPriceText = styled.p`
  font-size: 16px;
  font-weight: bold;
  margin-right: 5px;
  color: ${COLORS.MAIN_COLOR};
`;

const TotalPriceText = styled.p`
  ${props =>
    props.discountPrice &&
    css`
      text-decoration: line-through;
    `};
  font-size: ${props => (props.discountPrice ? '12px' : '16px')};
  font-weight: ${props => (props.discountPrice ? 'normal' : 'bold')};
`;

class CartModal extends Component {
  decrementProductQuantity = id => this.props.actions.decrementProductQuantity({ id });
  incrementProductQuantity = id => this.props.actions.incrementProductQuantity({ id });
  removeProductFromCart = id => {
    const productName = this.props.productsObject[id] ? this.props.productsObject[id].name : 'unknown';
    sendAmplitudeData('removeProductFromCart', { id, name: productName });
    this.props.actions.removeProductFromCart({ id });
  };

  renderProducts = () => {
    const { products } = this.props;
    if (products.length === 0) {
      return (
        <NoProductsContainer>
          <NoProductsText>Корзина пуста</NoProductsText>
        </NoProductsContainer>
      );
    }

    return (
      <ProductsContainer>
        {this.props.products.map((product, index) => (
          <div>
            {index !== 0 && <Separator />}
            <CartProduct
              product={product}
              onPlusPress={() => this.incrementProductQuantity(product.id)}
              onMinusPress={() => this.decrementProductQuantity(product.id)}
              onCrossPress={() => this.removeProductFromCart(product.id)}
            />
          </div>
        ))}
      </ProductsContainer>
    );
  };

  renderTotalPrice = () => {
    const { products } = this.props;
    if (products.length === 0) return false;

    const prices = products.reduce(
      (_prices, product) => ({
        normal: _prices.normal + product.price * product.quantity,
        discount: product.discountPrice
          ? _prices.discount + product.discountPrice * product.quantity
          : _prices.discount + product.price * product.quantity
      }),
      {
        normal: 0,
        discount: 0
      }
    );

    const { currency } = products[0];
    const discountPrice = prices.normal === prices.discount ? null : prices.discount;
    const price = prices.normal;

    return (
      <div>
        <Separator />
        <TotalPriceContainer>
          <TotalPriceLabelContainer>
            <TotalPriceLabel>Итого к оплате:</TotalPriceLabel>
          </TotalPriceLabelContainer>
          {discountPrice && (
            <TotalDiscountPriceText>{numberToCurrency(discountPrice, currency)}</TotalDiscountPriceText>
          )}
          <TotalPriceText discountPrice={discountPrice}>{numberToCurrency(price, currency)}</TotalPriceText>
        </TotalPriceContainer>
      </div>
    );
  };

  onCheckoutButtonClick = () => {
    sendAmplitudeData('onCheckoutButtonClick', getObjectFromCartProductsArray(this.props.cartProducts));
    this.props.close();
    this.props.history.push('/checkout/personal-info');
  };

  onCartDismiss = () => {
    sendAmplitudeData('onCartDismiss', getObjectFromCartProductsArray(this.props.cartProducts));
    this.props.close();
  };

  render() {
    return (
      <OuterContainer onClick={this.onCartDismiss} visible={this.props.visible}>
        <InnerContainer onClick={ev => ev.stopPropagation()} visible={this.props.visible}>
          <CloseIconContainer onClick={this.onCartDismiss}>
            <IoIosClose color={COLORS.LIGHT_GRAY} size={30} />
          </CloseIconContainer>

          <Header>Корзина</Header>
          {this.renderProducts()}
          {this.renderTotalPrice()}
          <Button
            disabled={this.props.products.length === 0}
            label='Оформить заказ'
            style={{ height: '42px' }}
            onClick={this.onCheckoutButtonClick}
            textStyle={{ fontSize: '12px' }}
          />
          <ContinueButtonContainer onClick={this.onCartDismiss}>
            <ContinueButtonText>Продолжить покупки</ContinueButtonText>
          </ContinueButtonContainer>
        </InnerContainer>
      </OuterContainer>
    );
  }
}

const mapStateToProps = state => {
  const cartProducts = state.cart.products.filter(product => state.products[product.id] != null);
  return {
    productsObject: state.products,
    cartProducts: cartProducts.map(product => ({
      ...product,
      name: state.products[product.id] ? state.products[product.id].name : 'unknown'
    })),
    products: cartProducts.map(product => ({
      ...state.products[product.id],
      quantity: product.quantity
    }))
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      incrementProductQuantity,
      decrementProductQuantity,
      removeProductFromCart
    },
    dispatch
  )
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(CartModal)
);

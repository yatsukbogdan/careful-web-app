import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { sendAmplitudeData } from '../../amplitude';
import { connect } from 'react-redux';
import { MdClear } from 'react-icons/md';

const OuterContainer = styled.div`
  z-index: ${props => (props.visible ? 300 : -1)};
  position: fixed;
  height: 100vh;
  width: 100vw;
  display: flex;
  transition: 0.4s;
  top: 0;
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  background-color: ${props => (props.visible ? 'rgba(0, 0, 0, 0.5)' : 'transparent')};
`;

const InnerContainer = styled.div`
  box-sizing: border-box;
  position: relative;
  justify-content: flex-end;
  display: flex;
  flex-direction: column;
  background-color: white;
  padding-bottom: 25px;
  height: 250px;
  width: 100%;
  transition: 0.4s;
  transform: ${props => (props.visible ? 'translate(0, 0)' : 'translate(0, -280px)')};
`;

const CloseIconContainer = styled.div`
  height: 50px;
  width: 50px;
  border-radius: 25px;
  background-color: white;
  align-items: center;
  justify-content: center;
  display: flex;
  position: absolute;
  bottom: -20px;
  transform: translate(-25px, 0);
  left: 50%;
`;

const Link = styled.p`
  font-size: 18px;
  text-align: center;
  margin-bottom: 15px;
`;

class Menu extends Component {
  onCategoryClick = id => {
    const categoryName = this.props.categories[id] ? this.props.categories[id].name : 'unknown';
    sendAmplitudeData('onCategoryClick', { id, name: categoryName });
    this.props.history.push(`/category/${id}`);
    this.props.close();
  };

  render() {
    const { visible, close, categoriesIds, categories } = this.props;

    return (
      <OuterContainer onClick={close} visible={visible}>
        <InnerContainer onClick={ev => ev.stopPropagation()} visible={visible}>
          <CloseIconContainer visible={visible} onClick={close}>
            <MdClear size={25} />
          </CloseIconContainer>
          {categoriesIds.map(id => (
            <Link onClick={() => this.onCategoryClick(id)}>{categories[id].name}</Link>
          ))}
        </InnerContainer>
      </OuterContainer>
    );
  }
}

const mapStateToProps = state => ({
  categoriesIds: Object.keys(state.categories),
  categories: state.categories
});

export default withRouter(connect(mapStateToProps)(Menu));

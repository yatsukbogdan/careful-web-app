import React, { Component } from 'react';
import styled from 'styled-components';
import { withRouter } from 'react-router';
import { IoLogoInstagram } from 'react-icons/io';
import { COLORS } from '../../config';
import { sendAmplitudeData } from '../../amplitude';

const Container = styled.div`
  background-color: ${COLORS.MAIN_COLOR};
  padding: 25px;
  display: flex;
  flex-direction: column;
`;

const HeaderLogoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
`;

const Text = styled.p`
  font-size: 20px;
  color: white;
`;

const Header = styled(Text)`
  letter-spacing: 1px;
  font-weight: 500;
`;

const Link = styled(Text)`
  font-weight: 200;
  margin-bottom: 10px;
`;

class Footer extends Component {
  navigateTo = link => {
    sendAmplitudeData('onFooterLinkClick', { link });
    this.props.history.push(link);
  };

  render() {
    const { visible, close } = this.props;

    return (
      <Container onClick={close} visible={visible}>
        <HeaderLogoContainer>
          <Header>Покупателям</Header>
          <a
            onClick={() => sendAmplitudeData('onFooterLinkClick', { link: '/instagram' })}
            href='https://www.instagram.com/careful.store/'
          >
            <IoLogoInstagram size={30} color='white' />
          </a>
        </HeaderLogoContainer>
        <Link onClick={() => this.navigateTo('/contacts')}>Контакты</Link>
        <Link onClick={() => this.navigateTo('/payment-and-delivery')}>Оплата и доставка</Link>
        <Link onClick={() => this.navigateTo('/privacy-policy')}>Политика конфиденциальности</Link>
        <Link onClick={() => this.navigateTo('/about-us')}>О нас</Link>
        <Link onClick={() => this.navigateTo('/contract-offer')}>Договор оферты</Link>
      </Container>
    );
  }
}

export default withRouter(Footer);

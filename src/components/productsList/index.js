import React, { Component } from 'react';
import Product from '../product';
import styled from 'styled-components';

const Container = styled.div`
  padding: 12.5%;
`;

const Separator = styled.div`
  height: 50px;
  background-color: white;
`;

const Label = styled.p`
  font-size: 24px;
  letter-spacing: 1px;
  text-align: center;
  margin-bottom: 50px;
`;
export default class ProductsList extends Component {
  static defaultProps = {
    label: null
  };

  render() {
    const { products, label } = this.props;
    const sortedProducts = products.sort((p1, p2) => p2.balance - p1.balance);

    return (
      <Container>
        {label !== null && <Label>{label}</Label>}
        {sortedProducts.map((product, index) => {
          if (index === 0) {
            return <Product product={product} />;
          }
          return (
            <div>
              <Separator />
              <Product product={product} />
            </div>
          );
        })}
      </Container>
    );
  }
}

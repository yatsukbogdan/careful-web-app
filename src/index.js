import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { initAmplitude } from './amplitude';
import API, { NovaPoshtaAPI } from './api';
import * as serviceWorker from './serviceWorker';
import * as Sentry from '@sentry/browser';

initAmplitude();
API.init();
NovaPoshtaAPI.init();
Sentry.init({ dsn: 'https://4a2cbeb0d6ba452aa765aaeb3bfdbd29@sentry.io/1402498' });

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
